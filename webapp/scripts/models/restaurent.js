let app = angular.module('app.model',[]);

app.factory('Restaurent', [()=> {
    class Restaurent {
        constructor(data) {
            const vm = this;
            vm.restaurentId = data.restaurentId;
            vm.imageUrl = data.imageUrl;
            vm.name = data.name;
        }
    }
    return Restaurent;
}]);
