var app = angular.module('app.controller', ['app.service','app.model']);
class RestaurentController {
    constructor(restaurentService,Restaurent) {
        const vm = this;
        vm.restaurentService = restaurentService;
        vm.restaurents = restaurentService.getRestaurents().then((data) => {
            vm.restaurents = data.data.Restaurents;
            vm.validRestaurents = [];
            vm.restaurents.forEach((restuarent)=> {
               vm.validRestaurents.push(new Restaurent(restuarent));
            });
        });
    }
}

app.component('restaurentComponent', {
    templateUrl: 'scripts/views/restaurent.html',
    controller: RestaurentController
});