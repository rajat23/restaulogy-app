let app =  angular.module('app.service',[]);

app.factory('restaurentService', ['$http', ($http) => {
     class RestaurentService {
        getRestaurents() {
            return $http({
                method: 'GET',
                url: 'http://localhost:8080/restaurent/',
                headers : {
                    'Access-Control-Allow-Origin': "*"
                }
            })
        }
    }
    return new RestaurentService();
}]);