module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            main: {
                files: [
                    // {src: ['scripts/*'], dest: '../src/main/resources/public/'},
                    {src: ['index.html'], dest: '../src/main/resources/public/'},
                    {src: ['node_modules/*/*'], dest: '../src/main/resources/public/'},
                    {src: ['scripts/**/*.html'], dest: '../src/main/resources/public/'},
                    {src: ['css/*'], dest: '../src/main/resources/public/'},
                    {src: ['img/*'], dest: '../src/main/resources/public/'}
                ]
            }
        },
        babel: {
            options: {
                sourceMap: true,
                presets: ['es2015']
            },
            dist: {
                files: [{
                    expand: true,
                    src: 'scripts/**/*.js',
                    dest: '../src/main/resources/public/'
                }]
            }
        },
        uglify: {
            my_target: {
                files: [{
                    expand: true,
                    src: 'scripts/*',
                    dest: '../src/main/resources/public',
                    mangle: false
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['babel']);
    grunt.registerTask('default', ['uglify']);
    grunt.registerTask('default', ['copy']);

};
