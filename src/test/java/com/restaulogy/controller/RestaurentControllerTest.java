package com.restaulogy.controller;

import com.restaulogy.model.Restaurent;
import com.restaulogy.model.RestaurentBuilder;
import com.restaulogy.model.Restaurents;
import com.restaulogy.service.RestaurentService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RestaurentControllerTest {
    @Mock
    private RestaurentService restaurentService;

    private RestaurentController restaurentController;

    @Before
    public void setUp() {
        initMocks(this);
        restaurentController = new RestaurentController(restaurentService);
    }

    @Test
    public void shouldReturnRestaurents() throws Exception {
        Restaurent restaurent = RestaurentBuilder.withDefaults().build();
        Restaurents expectedRestaurents = new Restaurents(restaurent);

        when(restaurentService.getRestaurents()).thenReturn(expectedRestaurents);

        Restaurents restaurants = restaurentController.restaurant();

        verify(restaurentService).getRestaurents();
        assertThat(restaurants, is(expectedRestaurents));
    }

    @Test
    public void shouldGetRestaurentById() throws Exception {
        String restaurentId = "restaurentId";
        Restaurent expectedRestaurent = RestaurentBuilder.withDefaults()
                .withRestaurentId(restaurentId)
                .build();

        when(restaurentService.getRestaurentBy(restaurentId)).thenReturn(expectedRestaurent);

        Restaurent restaurent = restaurentController.restaurentById(restaurentId);

        verify(restaurentService).getRestaurentBy(restaurentId);
        assertThat(restaurent, is(expectedRestaurent));
    }
}