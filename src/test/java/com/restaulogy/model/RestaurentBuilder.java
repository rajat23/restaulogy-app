package com.restaulogy.model;

import java.sql.Timestamp;

public final class RestaurentBuilder {
    private String restaurentId;
    private String name;
    private String imageUrl;
    private Timestamp endDate;

    private RestaurentBuilder() {
    }

    public static RestaurentBuilder withDefaults() {
        return new RestaurentBuilder()
                .withName("name")
                .withImageUrl("img.png")
                .withRestaurentId("restaurentId")
                .withEndDate(new Timestamp(0l));
    }

    public RestaurentBuilder withRestaurentId(String restaurentId) {
        this.restaurentId = restaurentId;
        return this;
    }

    public RestaurentBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public RestaurentBuilder withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public RestaurentBuilder withEndDate(Timestamp endDate) {
        this.endDate = endDate;
        return this;
    }

    public Restaurent build() {
        Restaurent restaurent = new Restaurent(restaurentId, name, imageUrl, endDate);
        return restaurent;
    }
}
