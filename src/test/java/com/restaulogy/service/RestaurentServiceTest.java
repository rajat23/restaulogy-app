package com.restaulogy.service;

import com.restaulogy.model.Restaurent;
import com.restaulogy.model.RestaurentBuilder;
import com.restaulogy.model.Restaurents;
import com.restaulogy.repository.AllRestaurents;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RestaurentServiceTest {
    @Mock
    private AllRestaurents allRestaurents;

    private RestaurentService restaurentService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        restaurentService = new RestaurentService(allRestaurents);
    }

    @Test
    public void shouldReturnAllRestaurentsWithEndDateNotNull() throws Exception {
        Restaurent restaurentOne = RestaurentBuilder.withDefaults()
                .withRestaurentId("restaurent1")
                .withEndDate(new Timestamp(0l))
                .build();
        Restaurent restaurentTwo = RestaurentBuilder.withDefaults()
                .withRestaurentId("restaurent2")
                .withEndDate(null)
                .build();

        List<Restaurent> expectedRestaurents = new Restaurents(restaurentOne, restaurentTwo);
        when(allRestaurents.findByEndDateNotNull()).thenReturn(expectedRestaurents);

        Restaurents restaurents = restaurentService.getRestaurents();

        verify(allRestaurents).findByEndDateNotNull();
        assertThat(restaurents, hasSize(1));
        assertThat(restaurents.get(0), is(restaurentOne));
    }

    @Test
    public void shouldReturnEmptyRestaurentListIfNoRestaurentFound() throws Exception {
        Restaurent restaurent = RestaurentBuilder.withDefaults().withEndDate(null).build();
        Restaurents expectedList = new Restaurents(restaurent);

        when(allRestaurents.findByEndDateNotNull()).thenReturn(expectedList);

        Restaurents restaurents = restaurentService.getRestaurents();

        verify(allRestaurents).findByEndDateNotNull();
        assertThat(restaurents, hasSize(0));
    }

    @Test
    public void getRestaurentById() throws Exception {
        String restaurentId = "restaurentId";
        Restaurent expectedRestaurent = null;
        when(allRestaurents.findByRestaurentId(restaurentId)).thenReturn(expectedRestaurent);

        Restaurent restaurent = restaurentService.getRestaurentBy(restaurentId);

        verify(allRestaurents).findByRestaurentId(restaurentId);
        assertThat(restaurent, is(expectedRestaurent));
    }
}