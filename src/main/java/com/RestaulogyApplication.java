package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.restaulogy.repository")
public class RestaulogyApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestaulogyApplication.class, args);
    }
    
}