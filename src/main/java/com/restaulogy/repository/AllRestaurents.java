package com.restaulogy.repository;

import com.restaulogy.model.Restaurent;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AllRestaurents extends org.springframework.data.repository.Repository<Restaurent, Integer> {

    Restaurent findByRestaurentId(String restaurentId);

    List<Restaurent> findByEndDateNotNull();
}