package com.restaulogy.model;

import java.util.ArrayList;

import static java.util.Arrays.asList;

public class Restaurents extends ArrayList<Restaurent> {
    public Restaurents() {
    }

    public Restaurents(Restaurent... restaurents) {
        super(asList(restaurents));
    }
}