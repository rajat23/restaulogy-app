package com.restaulogy.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = Restaurent.TBL_RESTAURENT)
public class Restaurent {
    static final String TBL_RESTAURENT = "tbl_restaurent";
    private static final String RESTAURENT_NAME = "restaurentName";
    private static final String RESTAURENT_IMG = "restaurentImg";
    private static final String RESTAURENT_END_DATE = "restaurentEndDate";
    private static final String DATETIME_FORMAT = "yyyy-mm-dd HH:MM:ss";

    @Id
    @JsonProperty
    private String restaurentId;

    @Column(name = RESTAURENT_NAME)
    @JsonProperty
    private String name;

    @JsonProperty
    @Column(name = RESTAURENT_IMG)
    private String imageUrl;

    @JsonProperty
    @Column(name = RESTAURENT_END_DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATETIME_FORMAT)
    private Timestamp endDate;

    public Restaurent() {
    }

    public Restaurent(String restaurentId, String name, String imageUrl, Timestamp endDate) {
        this.restaurentId = restaurentId;
        this.name = name;
        this.imageUrl = imageUrl;
        this.endDate = endDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }
}
