package com.restaulogy.service;

import com.restaulogy.model.Restaurent;
import com.restaulogy.model.Restaurents;
import com.restaulogy.repository.AllRestaurents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurentService {
    private AllRestaurents allRestaurents;

    @Autowired
    public RestaurentService(AllRestaurents allRestaurents) {
        this.allRestaurents = allRestaurents;
    }

    public Restaurents getRestaurents() {
        List<Restaurent> restaurents = allRestaurents.findByEndDateNotNull();
        Restaurents validRestaurents = new Restaurents();
        for (Restaurent restaurent : restaurents) {
            if (restaurent.getEndDate() != null) {
                validRestaurents.add(restaurent);
            }
        }
        return validRestaurents;
    }

    public Restaurent getRestaurentBy(String restaurentId) {
        return allRestaurents.findByRestaurentId(restaurentId);
    }
}
