package com.restaulogy.controller;

import com.restaulogy.model.Restaurent;
import com.restaulogy.model.Restaurents;
import com.restaulogy.service.RestaurentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/restaurent", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestaurentController {
    private RestaurentService restaurentService;

    @Autowired
    public RestaurentController(RestaurentService restaurentService) {
        this.restaurentService = restaurentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Restaurents restaurant() {
        return restaurentService.getRestaurents();
    }

    @RequestMapping(value = "/{restaurentId}", method = RequestMethod.GET)
    public Restaurent restaurentById(@PathVariable String restaurentId) {
        return restaurentService.getRestaurentBy(restaurentId);
    }
}

